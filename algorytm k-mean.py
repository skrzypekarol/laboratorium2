import csv
import numpy as np
from sklearn.cluster import KMeans

points = []
with open('points.xyz', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        x, y, z = map(float, row)
        points.append([x, y, z])

points = np.array(points)
kmeans = KMeans(n_clusters=3, random_state=0).fit(points)
labels = kmeans.labels_
clustered_points = [points[labels == i] for i in range(3)]

print(f'Liczba punktów: {len(points)}')
for i, cluster in enumerate(clustered_points):
    print(f'Chmura punktów {i+1} (liczba punktów: {len(cluster)}):')
    print(cluster)