import csv
import numpy as np
from sklearn.cluster import KMeans
from sklearn.linear_model import RANSACRegressor
import pandas as pd


# Otwieranie pliku csv
with open('points.xyz', newline='') as csvfile:
    # Tworzenie czytnika csv
    csv_reader = csv.reader(csvfile, delimiter=',')
    # Tworzenie listy punktów
    points = []
    for row in csv_reader:
        x = float(row[0])
        y = float(row[1])
        z = float(row[2])
        points.append([x, y, z])

# Konwertowanie listy punktów na obiekt DataFrame z biblioteki Pandas
df = pd.DataFrame(points, columns=['x', 'y', 'z'])

# Tworzenie modelu k-średnich
kmeans = KMeans(n_clusters=3)

# Uczenie modelu na danych
kmeans.fit(df)

# Przypisanie klastrów do punktów
labels = kmeans.predict(df)

# Dopasowanie płaszczyzn RANSAC do każdej chmury
for i in range(3):
    print("Chmura punktów", i+1)
    points_in_cluster = df[labels == i]
    x = points_in_cluster[['x', 'y']]
    y = points_in_cluster['z']
    ransac = RANSACRegressor()
    ransac.fit(x, y)
    inlier_mask = ransac.inlier_mask_
    outlier_mask = np.logical_not(inlier_mask)
    # Wyznaczanie wektora normalnego do płaszczyzny
    coef = ransac.estimator_.coef_
    normal_vector = np.array([coef[0], coef[1], -1])
    # Określanie, czy chmura punktów jest płaszczyzną
    mean_distance = np.mean(np.abs(ransac.predict(x[inlier_mask]) - y[inlier_mask]))
    print("Średnia odległość od płaszczyzny: ", mean_distance)
    if mean_distance < 0.01:
        print("Chmura punktów jest płaszczyzną.")
        # Określanie, czy płaszczyzna jest pionowa czy pozioma
        angle_to_vertical = np.arccos(np.abs(normal_vector[2]) / np.linalg.norm(normal_vector)) * 180 / np.pi
        if angle_to_vertical < 10:
            print("Płaszczyzna jest pionowa.")
        elif angle_to_vertical > 80:
            print("Płaszczyzna jest pozioma.")
        else:
            print("Płaszczyzna jest skośna.")
    else:
        print("Chmura punktów nie jest płaszczyzną.")
    print("Wektor normalny do płaszczyzny: ", normal_vector)
